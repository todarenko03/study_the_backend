from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/data', methods=['POST'])
def data():
    string = request.args.get('string')
    file = open('data/file.txt', 'w')
    file.write(string)
    file.close()
    file2 = open('data/file.txt', 'r')
    res = file2.read()
    file2.close()
    return res

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)


