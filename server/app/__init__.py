from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/data', methods=['POST'])
def data():
    string = request.args.get('string')
    file = open('data/file.txt', 'w+')
    file.write(string)
    file.close()
    
    return '<h1>Hello!</h1>'

if __name__ == "__main__":
    app.run(debug=True)


